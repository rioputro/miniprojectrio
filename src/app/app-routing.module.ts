import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FirstTaskComponent} from "./first-task/first-task.component";
import {SecondTaskComponent} from "./second-task/second-task.component";
import {ThirdTaskComponent} from "./third-task/third-task.component";
import {ListDoctorComponent} from "./third-task/list-doctor/list-doctor.component";
import {DetailDoctorComponent} from "./third-task/detail-doctor/detail-doctor.component";

const routes: Routes = [
  {
    path: 'task-1',
    component: FirstTaskComponent
  },
  {
    path: 'task-2',
    component: SecondTaskComponent
  },
  {
    path: 'task-3',
    children: [
      {
        path: '',
        component: ThirdTaskComponent,
      },
      {
        path: 'list-doctor',
        children: [
          {
            path: ':latitude/:longitude',
            component: ListDoctorComponent
          }
        ]
      },
      {
        path: 'detail-doctor',
        children : [
          {
            path: ':id',
            component: DetailDoctorComponent
          }
        ]
      }
    ]
  },
  {
    path: '',
    redirectTo: '/task-1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
