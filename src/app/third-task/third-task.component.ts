import {Component, Inject, OnInit} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {environment} from "../../environments/environment";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-third-task',
  template: `
    <div class="container w-96 flex flex-1 flex-col justify-center">
      <h1 class="text-2xl mb-2 text-neutral">FIND DOCTOR</h1>

      <form [formGroup]="form" (ngSubmit)="search()">
        <div id="maps-search" class="relative">
          <input id="maps-input" class="controls input input-bordered w-full pr-8" type="text"
                 placeholder="Search area..." formControlName="areaSearch">
          <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 absolute top-3.5 bottom-3.5 right-2.5"
               viewBox="0 0 20 20"
               fill="currentColor">
            <path fill-rule="evenodd"
                  d="M8 4a4 4 0 1 0 0 8 4 4 0 0 0 0-8zM2 8a6 6 0 1 1 10.89 3.476l4.817 4.817a1 1 0 0 1-1.414 1.414l-4.816-4.816A6 6 0 0 1 2 8z"
                  clip-rule="evenodd"/>
          </svg>
        </div>

        <div id="infowindow-content" class="flex flex-col my-4">
          <span id="place-name" class="title text-xl text-bold"></span>
          <span id="place-address" class="text-xs"></span>
        </div>
        <button class="flex justify-center self-end btn btn-neutral w-1/2" type="submit" [disabled]=!form.valid>
          Find Doctor
        </button>
      </form>

    </div>
  `,
})
export class ThirdTaskComponent implements OnInit {
  loadMap: any;
  window: any = window;

  autoComplete: google.maps.places.Autocomplete;
  infoWindow: google.maps.InfoWindow;
  place: google.maps.places.PlaceResult;

  form: FormGroup;

  constructor(@Inject(DOCUMENT) public document: Document,
              public fb: FormBuilder,
              private router: Router) {
  }

  ngOnInit(): void {
    this.loadGoogleMapsApi();
    this.checkGoogleMaps();
    this.initForm();
  }

  loadGoogleMapsApi(): void {
    const googleMapScript = this.document.getElementById('google-map-script') as HTMLElement;
    if (!googleMapScript) {
      const newGoogleMapScript = this.document.createElement('script');
      newGoogleMapScript.setAttribute('id', 'google-map-script');
      newGoogleMapScript.setAttribute('src',
        'https://maps.googleapis.com/maps/api/js?key=' + environment.GOOGLE_MAPS_API_KEY + '&libraries=places');
      this.document.body.appendChild(newGoogleMapScript);
    }
  }

  initSearch(): void {
    this.setupAutocomplete();

    const infowindowContent = document.getElementById(
      "infowindow-content"
    ) as HTMLElement;
    this.setupInfoWindow(infowindowContent);

    this.autoComplete.addListener("place_changed", () => {
      this.infoWindow.close();

      this.place = this.autoComplete.getPlace();

      if (!this.place.geometry || !this.place.geometry.location) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert("No details available for input: '" + this.place.name + "'");
        return;
      }

      infowindowContent.children["place-name"].textContent = this.place.name;
      infowindowContent.children["place-address"].textContent = this.place.formatted_address;
    });
  }

  checkGoogleMaps(): void {
    this.loadMap = setTimeout(() => {
      if (typeof this.window.google === 'object' && typeof this.window.google.maps === 'object') {
        this.initSearch();
      } else {
        throw new Error('Failed to connect to Google Maps.');
      }
    }, 1000);
  }

  setupAutocomplete(): void {
    const options = {
      fields: ["formatted_address", "geometry", "name"],
      strictBounds: false,
      types: ["establishment"],
      componentRestrictions: {country: "id"}
    };

    const input = document.getElementById("maps-input") as HTMLInputElement;
    this.autoComplete = new google.maps.places.Autocomplete(input, options);
    this.autoComplete.setFields(["place_id", "geometry", "name"]);
  }

  setupInfoWindow(infowindowContent: HTMLElement): void {
    this.infoWindow = new google.maps.InfoWindow();
    this.infoWindow.setContent(infowindowContent);
  }

  search(): void {
    this.router.navigate([`task-3/list-doctor/${this.place.geometry.location.lat().toString()}/${this.place.geometry.location.lng().toString()}`]);
  }

  initForm(): void {
    this.form = this.fb.group({
      areaSearch: ['', [Validators.required]],
    })
  }
}
