import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {DoctorService} from "./doctor.service";
import {Doctor} from "../../doctor";
import {HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-list-doctor',
  templateUrl: './list-doctor.component.html',
  styleUrls: ['./list-doctor.component.scss']
})
export class ListDoctorComponent implements OnInit {
  lat: string;
  lng: string;

  listDoctor: Doctor[];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private service: DoctorService) {
  }

  ngOnInit(): void {
    this.lat = this.route.snapshot.paramMap.get('latitude');
    this.lng = this.route.snapshot.paramMap.get('longitude');

    this.service.getDoctorList(this.lat, this.lng).subscribe((doctors) => {
      if (doctors) {
        this.listDoctor = doctors;
      }
    })
  }

  goToDetail(id: number) {
    this.router.navigate([`task-3/detail-doctor/${id}`]);
  }
}
