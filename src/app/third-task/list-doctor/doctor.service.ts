import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from "@angular/common/http";
import {Doctor} from "../../doctor";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  constructor(private http: HttpClient) { }

  getDoctorList(lat: string, lng: string): Observable<Doctor[]> {
    return this.http.get <Doctor[]>(`/api/doctors?latitude=${lat}&longitude=${lng}&_page=1&_limit=10`, {
      responseType: 'json'
    })
  }

  getDoctorDetail(id: number): Observable<Doctor> {
    return this.http.get<Doctor>(`/api/doctors?id=${id}`, {
      responseType: 'json'
    })
  }
}
