import {Component, Inject, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {DoctorService} from "../list-doctor/doctor.service";
import {Doctor} from "../../doctor";
import {environment} from "../../../environments/environment";
import {DOCUMENT} from "@angular/common";

@Component({
  selector: 'app-detail-doctor',
  templateUrl: './detail-doctor.component.html',
  styleUrls: ['./detail-doctor.component.scss']
})
export class DetailDoctorComponent implements OnInit {
  doctorId: number;
  doctor: Doctor;

  loadMap: any;
  window: any = window;

  map: google.maps.Map;

  constructor(@Inject(DOCUMENT) public document: Document,
              private route: ActivatedRoute,
              private service: DoctorService) {
  }

  ngOnInit(): void {
    this.doctorId = Number(this.route.snapshot.paramMap.get('id'));

    this.loadGoogleMapsApi();
    this.checkGoogleMaps();
  }

  loadGoogleMapsApi(): void {
    const googleMapScript = this.document.getElementById('google-map-script') as HTMLElement;
    if (!googleMapScript) {
      const newGoogleMapScript = this.document.createElement('script');
      newGoogleMapScript.setAttribute('id', 'google-map-script');
      newGoogleMapScript.setAttribute('src',
        'https://maps.googleapis.com/maps/api/js?key=' + environment.GOOGLE_MAPS_API_KEY + '&libraries=places');
      this.document.body.appendChild(newGoogleMapScript);
    }
  }

  setDoctorLocation(): void {
    const center: google.maps.LatLngLiteral = {lat: this.doctor.latitude, lng: this.doctor.longitude};

    this.map = new google.maps.Map(document.getElementById("map") as HTMLElement, {
      center,
      zoom: 16
    });
  }

  checkGoogleMaps(): void {
    this.loadMap = setTimeout(() => {
      if (typeof this.window.google === 'object' && typeof this.window.google.maps === 'object') {
        this.fetchDetailDoctor();
      } else {
        throw new Error('Failed to connect to Google Maps.');
      }
    }, 1000);
  }

  fetchDetailDoctor(): void {
    this.service.getDoctorDetail(this.doctorId).subscribe((doctor) => {
      if (doctor) {
        this.doctor = doctor[0];
        this.setDoctorLocation();
      }
    })
  }
}
