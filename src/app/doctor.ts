export interface Doctor {
  id: number,
  name: string,
  title: string,
  cost: number,
  description: string,
  image: string,
  latitude: number,
  longitude: number
}
