import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-first-task',
  template: `
    <div class="container w-3/6 flex flex-1 flex-col justify-center items-center">
      <h1 class="text-2xl mb-2 text-neutral">COUNTING NUMBERS</h1>
      <p class="text-xs mb-8">Insert a number (n) and it will be counted with formula : n*(n-1)*(n-2)...*(n(n-1))</p>

      <form [formGroup]="form" (ngSubmit)="count()">
        <div class="form-control">
          <label class="label">
            <span class="label-text">Number</span>
          </label>
          <input type="text" formControlName="myNumber" placeholder="insert number" class="input input-bordered">
          <button class="btn my-4" type="submit" [disabled]="!form.valid">count</button>
        </div>
      </form>

      <h1 class="text-xl" *ngIf="result">Result : {{result}}</h1>

    </div>
  `,
})
export class FirstTaskComponent implements OnInit {

  form: FormGroup;
  result: number;

  constructor(public fb: FormBuilder) {
  }

  get myNumber(): FormControl {
    return this.form.get('myNumber') as FormControl;
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      myNumber: ['', [
        Validators.required,
        Validators.pattern(/^[1-9]\d*$/),
      ]]
    });
  }

  count(): void {
    if (!!this.myNumber.value) {
      this.result = this.factorial(this.myNumber.value);
    }
  }

  factorial(num: number): number {
    let tempResult = num;

    while (num > 1) {
      num--;
      tempResult *= num;
    }

    return tempResult;
  }
}
