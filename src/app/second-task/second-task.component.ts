import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-second-task',
  template: `
    <div class="container w-3/6 flex flex-1 flex-col justify-center items-center">
      <h1 class="text-2xl mb-2 text-neutral">FIND REPEATED CHARACTER</h1>
      <p class="text-xs mb-8">Find most repeated character in a string/word</p>

      <form [formGroup]="form" (ngSubmit)="count()">
        <div class="form-control">
          <label class="label">
            <span class="label-text">Word</span>
          </label>
          <input type="text" formControlName="myWord" placeholder="insert word/string" class="input input-bordered">
          <button class="btn my-4" type="submit" [disabled]="!form.valid">count</button>
        </div>
      </form>

      <h1 class="text-xl" *ngIf="result">Result : {{result}}</h1>

    </div>

  `,
})
export class SecondTaskComponent implements OnInit {

  form: FormGroup;
  result: string;

  constructor(public fb: FormBuilder) {
  }

  get myWord(): FormControl {
    return this.form.get('myWord') as FormControl;
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      myWord: ['', [
        Validators.required,
        Validators.pattern(/^[a-zA-Z]\w*$/),
      ]]
    });
  }

  count(): void {
    if (this.myWord.value) {
      this.result = this.countRepeatedCharacter(this.myWord.value);
    }
  }

  countRepeatedCharacter(word: string): string {
    const distinctWord = [...new Set(word.split(''))];
    const sortedWord = word.split('').sort().toString();

    let wordList = {};
    let res = 0;
    let repeatedChar = '';

    for (const w of distinctWord) {
      const re = new RegExp(`(?=)[${w}]+`, 'g');
      wordList[w] = sortedWord.match(re).length;

      if (res < wordList[w]) {
        res = wordList[w];
        repeatedChar = w;
      }
    }

    return repeatedChar;
  }
}
